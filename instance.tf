resource "aws_key_pair" "iac-fatesg-ssh-key" {
  key_name   = "iac-fatesg-ssh-key"
  public_key = file("./keys/iac-fatesg.pub")
}

data "aws_ami" "image-iac-fatesg" {
  most_recent = true
  owners = ["309956199498"] // Red Hat's Account ID
  filter {
    name   = "name"
    values = ["RHEL-8.5*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "ansible-node-vm" {
  ami           = data.aws_ami.image-iac-fatesg.id
  instance_type = "t2.micro"

  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.iac-fatesg-sg.id]

  key_name = aws_key_pair.iac-fatesg-ssh-key.key_name
  tags     = { Name = "ansible-node-vm" }

  user_data = file("./files/script.sh")
}

resource "aws_instance" "instance01-vm" {
  ami           = data.aws_ami.image-iac-fatesg.id
  instance_type = "t2.micro"

  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.iac-fatesg-sg.id]

  key_name = aws_key_pair.iac-fatesg-ssh-key.key_name
  tags     = { Name = "instance01-vm" }
}

resource "aws_instance" "instance02-vm" {
  ami           = data.aws_ami.image-iac-fatesg.id
  instance_type = "t2.micro"

  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.iac-fatesg-sg.id]

  key_name = aws_key_pair.iac-fatesg-ssh-key.key_name
  tags     = { Name = "instance02-vm" }
}

output "ips-instancias" {
  value = [ 
    aws_instance.ansible-node-vm.public_ip,
    aws_instance.instance01-vm.public_ip,
    aws_instance.instance02-vm.public_ip
  ]
}
