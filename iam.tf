resource "aws_iam_group" "devops-fatesg" {
  name = "devops-fatesg"
}

resource "aws_iam_group_policy" "full_policy" {
  name   = "full_policy"
  group  = aws_iam_group.devops-fatesg.id
  policy = file("./files/policy.json")
}

resource "aws_iam_user_group_membership" "group-devsecops" {
  user   = "iac-fatesg"
  groups = [aws_iam_group.devops-fatesg.name]
}
