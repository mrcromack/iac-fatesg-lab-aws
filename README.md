# Provisionamento de Laboratório - Disciplina IaC SENAI FATESG

Saudações pessoal!

Este repositório contém os arquivos do Terraform necessários para provisionar a infraestrutura utilizada na aula de IaC do curso de Cloud Computing do SENAI FATESG.

Siga as instruções abaixo para configurar e provisionar a infraestrutura.

## Clonar o repositório

Comece clonando este repositório executando o seguinte comando:

```shell
git clone https://gitlab.com/o_sgoncalves/iac-fatesg-lab-aws.git
```

Após a clonagem, acesse o diretório criado com o comando:

```shell
cd iac-fatesg-lab-aws
```

## Configurar o provedor

Agora que você clonou o repositório e está no diretório correto, edite o arquivo `provider.tf` e insira suas credenciais da AWS. O arquivo deve ficar parecido com o exemplo abaixo:

```hcl
provider "aws" {
    region = "us-east-1"
    access_key = "SUA_ACCESS_KEY"
    secret_key = "SUA_SECRET_ACCESS_KEY"
}
```

> ### **Observação importante**
> Crie um usuário no painel da AWS chamado "iac-fatesg" exatamente desta forma, com as permissões:
> - IAMFullAccess
> - EC2FullAccess

## Aplicar o Terraform

Antes de tudo, inicialize o Terraform no diretório atual:

```shell
terraform init
```

Em seguida, planeje a implantação:

```shell
terraform plan
```

Agora, execute a aplicação:

```shell
terraform apply
```

Após completar esses passos, sua infraestrutura estará pronta para a aula.

Abraços!